﻿using BankApi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankApi.IRepositories
{
    public interface IAccountRepository
    {
        Task<List<Account>> GetAccounts();
        Task<Account> GetAccountById(string Id);
        Task<bool> AddAccount(Account account);
        Task<bool> UpdateAccount(Account account);
        Task<bool> DeleteAccount(string Id);
    }
}
