﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BankApi.Model
{
    public class Account
    {
        [Required(ErrorMessage = "ID is requird")]
        public string Id { get; set; }

        [Required(ErrorMessage = "Name is requird")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Balance is requird")]
        public double Balance { get; set; }

        [Required(ErrorMessage = "Branch is requird")]
        public string Branch { get; set; }

        [Required(ErrorMessage = "Owner is requird")]
        public string Owner { get; set; }
    }
}
