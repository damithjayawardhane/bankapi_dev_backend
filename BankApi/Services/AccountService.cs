﻿using BankApi.IRepositories;
using BankApi.IServices;
using BankApi.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankApi.Services
{
    public class AccountService : IAccountService
    {
        IConfiguration configuration;
        IAccountRepository accountRepository;
        public AccountService(IConfiguration _configuration, IAccountRepository _accountRepository)
        {
            configuration = _configuration;
            accountRepository = _accountRepository;
        }
        public async Task<bool> AddAccount(Account account)
        {
            var addacc = await accountRepository.AddAccount(account);
            return addacc;
        }

        public async Task<bool> DeleteAccount(string Id)
        {
            var delacc = await accountRepository.DeleteAccount(Id);
            return delacc;
        }

        public async Task<Account> GetAccountById(string Id)
        {
            var Get_Acc_Id = await accountRepository.GetAccountById(Id);
            return Get_Acc_Id;
        }

        public async Task<List<Account>> GetAccounts()
        {
            var Get_Acc_All = await accountRepository.GetAccounts();
            return Get_Acc_All;
        }

        public async Task<bool> UpdateAccount(Account account)
        {
            var updateAcc = await accountRepository.UpdateAccount(account);
            return updateAcc;
        }
    }
    
}
